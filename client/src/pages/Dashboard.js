import React, { useEffect, useState } from 'react'
import jwt from 'jsonwebtoken'
import { useHistory } from 'react-router-dom'

const Dashboard = () => {
	const history = useHistory()
	const [citationKaamelott, setCitationKaamelott] = useState('')
	const [tempCitationKaamelott, setTempCitationKaamelott] = useState('')

	useEffect(() => {
		const token = localStorage.getItem('token')
		if (token) {
			console.log(token)
			const user = jwt.decode(token)
			if (!user) {
				localStorage.removeItem('token')
				history.replace('/login')
			} else {
				CitationKaamelott()
			}
		}
	}, [])

	async function CitationKaamelott() {
		const req = await fetch('http://localhost:4001/api/citationKaamelott', {
			headers: {
				'x-access-token': localStorage.getItem('token'),
			},
		})

		const data = await req.json()
		if (data.status === 'ok') {
			setCitationKaamelott(data.citationKaamelott)
		} else {
			alert(data.error)
		}
	}


	async function updateCitationKaamelott(event) {
		event.preventDefault()

		const req = await fetch('http://localhost:4001/api/citationKaamelott', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'x-access-token': localStorage.getItem('token'),
			},
			body: JSON.stringify({
				citationKaamelott: tempCitationKaamelott,
			}),
		})

		const data = await req.json()
		if (data.status === 'ok') {
			setCitationKaamelott(tempCitationKaamelott)
			setTempCitationKaamelott('')
		} else {
			alert(data.error)
		}
	}

	return (
		<div>
			<h1>Votre citation de Kaamelott favorite : </h1><br/>
			<p>{citationKaamelott || "Moi je bondis, comme ça là, après je vous arrive dessus en piqué diagonale et là c'est l'hymne à la cruauté, attention : un autel dressé au culte de la barbarie !"}</p>
			<form onSubmit={updateCitationKaamelott}>
				<input type="text" placeholder="..."
					   value={tempCitationKaamelott} onChange={(e) => setTempCitationKaamelott(e.target.value)}
				/>
				<input type="submit" value="Modifier citation" />
			</form>
		</div>
	)
}

export default Dashboard