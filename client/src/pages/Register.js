import { useState } from 'react'
import { useHistory } from 'react-router-dom'

function App() {
	const history = useHistory()
	const [name, setName] = useState('')
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')

	async function registerUser(event) {
		event.preventDefault()

		const response = await fetch('http://localhost:4001/api/register', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({name, email, password,
			}),
		})

		const data = await response.json()

		if (data.status === 'ok') {
			history.push('/login')
		} else {
			console.log(response);
			alert('incorrect')
		}
	}

	return (
		<div>
			<h1>Enregistrer un compte</h1>
			<form onSubmit={registerUser}>
				<input
					value={name}
					onChange={(e) => setName(e.target.value)}
					type="text"
					placeholder="Nom"
				/>
				<br/>
				<input
					value={email}
					onChange={(e) => setEmail(e.target.value)}
					type="email"
					placeholder="Email"
				/>
				<br />
				<input
					value={password}
					onChange={(e) => setPassword(e.target.value)}
					type="password"
					placeholder="Password"
				/>
				<br/>
				<input type="submit" value="Enregistrer"/>
			</form>
		</div>
	)
}

export default App
